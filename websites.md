# Useful websites

Here is a list of websites which can be helpful at times, either for their payloads or tutorials.

## Payloads

[GTFOBins](https://gtfobins.github.io/) - "A curated list of Unix binaries that can be used to bypass local security restrictions in misconfigured systems."

[revshells.com](https://revshells.com) - Reverse shell generator. 

[PayloadAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings) - Large list of useful Payloads. 
Their [reverse shell cheatsheet](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md) is specially useful.


## Tutorials

[Hacktricks](https://book.hacktricks.xyz/) - Website with tricks, tips and others - made by Carlos Polop.

[Hacker's Grimoire](https://vulp3cula.gitbook.io/hackers-grimoire) - A fellow penetration tester notes.

[Kali documentation](https://www.kali.org/tools/) - Kali Linux documentation, sometimes they have some useful use-case examples.

[h4cker](https://github.com/The-Art-of-Hacking/h4cker) - Another repository full of learning resources.

## Search engines

#### Attack surface
[app.netlas.io](https://app.netlas.io/)

#### Certificate search
[crt.sh](https://crt.sh/)

#### Code
[grep.app](https://grep.app/)

[searchcode.com](https://searchcode.com/)

#### Email addresses
[hunter.io](https://hunter.io/)

#### OSINT
[intelx.io](https://intelx.io/)

#### Threat Intelligence
[viz.greynoise.io](https://viz.greynoise.io/)

[fofa.info](https://en.fofa.info/)

[leakix.net](https://leakix.net/)

[urlscan.io](https://urlscan.io/)

[onyphe.io](https://www.onyphe.io/)

[shodan.io](https://www.shodan.io/)


#### Wifi Networks
[wigle.net](https://wigle.net/)


