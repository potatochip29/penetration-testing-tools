# Pwn and Reverse-engineering
This file is a list of tools helpful when dealing with binary exploitation and reverse engineering.

---

### [Ghidra](https://github.com/NationalSecurityAgency/ghidra)
Software Reverse Engineering Framework and a free alternative to IDA Pro.

---

### [Pwntools](https://docs.pwntools.com/en/stable/)
Python CTF framework and exploit development library.
[This link is a good starting point.](https://docs.pwntools.com/en/stable/intro.html)

---

### checksec
A bash script to check the properties of executables (like PIE, RELRO, Canaries, ASLR, Fortify Source).
```bash
checksec --file=executablefile
```
To get a json instead (note that the output will not have the warning colors).
```bash
checksec --file=executablefile --output=json | jq
```

---

### readelf
Displays information about one or more ELF format object files.
```bash
readelf -W --symbols executablefile
```

---

### hexeditor
Full screen curses Hex editor.
```bash
hexeditor executablefile
```

---

### ltrace 
Useful program to record dynamic library calls of a process, signals received by the process and system calls executed by the program.
```
ltrace ./executablefile
```

