
# Credentials

This is a list of useful tools to find your way past a login.

-------------------------------------------------------------------

### John the ripper
Useful password/hash cracker.
Needs to have cuda installed.
```bash
john --wordlist=/usr/share/dict/rockyou.txt hashes.txt
```
-------------------------------------------------------------------

### Hashcat
Another password/hash cracker.

-------------------------------------------------------------------

### [hash-id](https://github.com/Tashima42/hash-id)
Hash identifier - given a hash, tries to identify the hashing method.
```bash
hash-id -f hashes.txt
```
-------------------------------------------------------------------

### rockyou
Popular credentials list. 
Can be found in SecLists under `/usr/share/seclists/Passwords/Leaked-Databases/rockyou.txt`, along with many other lists.

-------------------------------------------------------------------

### mkpasswd
Not a password cracker. Easy way to hash/encrypt a password, usually to add to some database to get a working login.
```bash
mkpasswd --method=sha-512 bolas
```

