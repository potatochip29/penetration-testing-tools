
# Enumeration
This file is a list of tools to enumerate possible attack vectors.

---

### nmap 
Utility to find services running on the target machine. 
Detects open ports and respective services.

To detect open ports ranging from 0 to 10000, find the software version with intensity 0 on a target machine:
```bash
nmap --version-intensity 0 -p-10000 10.129.1.15
```

To detect commonly used open ports, print a status as it runs and attempt to use it's own scripts (sC):
```bash
nmap -sC -sV 10.129.154.11 --stats-every 10s
```

---

### rustscan
Same thing as nmap, but faster and less thorough.
Useful to get started while nmap is still running in the background.
```bash
rustscan -a 10.129.136.7
```

---

### gobuster
Utility for both subdomain and url enumeration, plus others.

To find subdomains on a given target machine. Note that an ip can be used instead.
```bash
gobuster vhost --wordlist subdomains-top1million-5000.txt --url thetoppers.htb
```

To find common subdirectories, with a `.php` extension:
```
gobuster dir --url 10.129.107.39 -x .php --wordlist directory-list-2.3-small.txt
```

---

### [PEASS-ng project](https://github.com/carlospolop/PEASS-ng)
Executable that enumerates possible vectors of attack in order to escalate privileges in a machine.
There are both a Linux and Windows versions, [linPEAS](https://github.com/carlospolop/PEASS-ng/tree/master/linPEAS) and [winPEAS](https://github.com/carlospolop/PEASS-ng/tree/master/winPEAS) respectively.

---

### [Pspy](https://github.com/DominicBreuker/pspy/blob/master/README.md)
Command line tool designed to snoop on processes without need for root permissions

