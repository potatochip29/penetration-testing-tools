# Penetration Testing Tools

This repository serves as my personal list of notes taken while doing penetration testing challenges, so that I can have quick access to anything I may need.

**It also serves as a personal portfolio of tools I have experience with.**
However, **this is not a complete list** of tools I have used, just a list of tools I've had contact with since I started honing my pentest skills recently.

In some commands [I may reference one of SecLists' lists](https://github.com/danielmiessler/SecLists), which are useful for enumeration, for example.
Can be found in `/usr/share/seclists` once installed in Linux.

----

## Lists of tools I have already used

**[Enumeration](tools_lists/enumeration.md)**

**[Exploitation](tools_lists/exploitation.md)**

**[Login bypassing](tools_lists/credentials.md)**

**[Pwn and Reverse-engineering](tools_lists/pwn_and_reverse.md)**

**[Other](tools_lists/other.md)**

----

## Other stuff I did

**[How to upgrade your shell](get_full_ttys.md)**, a short tutorial.

**[List of infosec websites](websites.md)**, a list I have been compiling.

**[My public write-ups repository](https://gitlab.com/potatochip29/challenge-write-ups)**, a repo where you check what challenges I have done recently.

**[My private write-ups repository](https://gitlab.com/potatochip29/private-write-ups)**, a repo which you can access _if you are me_.





